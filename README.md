Entropy measure of abstract syntax trees (AST).
Converts Python and C/C++ (LLVM) source code to an AST and counts the regularity of similar tree samples, i.e., the similar code segments.
Taking the substrees of different sizes as information theory symbols, it computes the entropy of such symbols,
thus a measure is given that penalizes repeated code segments of different size.

Usage: `code_entropy.py <lang> [-max <K>] <file1> [<file2>] ...`
Example: `code_entropy.py c++ -max 5 cpp_file1.cc cpp_file2.cc cpp_file3.cc ...`
    where -max defines the maximum size of tree samples to account for
