from tree_sample_entropy import *
import ast


# apart from the node type name, extra information is appended to the id of the node
# this extra information is retrieved from the node according to its type
# according to https://greentreesnakes.readthedocs.io/en/latest/nodes.html
py3_node_ids = ['FormattedValue', 'JoinedStr', 'NameConstant']
for node_id in py3_node_ids:
    if not hasattr(ast, node_id):
        setattr(ast, node_id, None)
PYNODE_TO_NODE_ID_INFO = {
    ast.Str: lambda node: str(hash(node.s)),
    ast.Name: lambda node: node.id,
    ast.keyword: lambda node: node.arg,
    ast.Attribute: lambda node: node.attr,
    ast.ImportFrom: lambda node: node.module,
    ast.alias: lambda node: node.name,
    ast.FunctionDef: lambda node: node.name,
    ast.Global: lambda node: ' '.join(node.names),
    ast.ClassDef: lambda node: node.name,
    ast.Num: lambda node: str(node.n),
    ast.Ellipsis: lambda node: None,
    ast.Pass: lambda node: None,
    ast.FormattedValue: lambda node: node.conversion,
    ast.JoinedStr: lambda node: None,
    ast.NameConstant: lambda node: str(node.value)
}


def py_load_node_id(node):
    """Converts Python ast.AST to ASTnode."""
    node_id = type(node).__name__ + ' '
    if type(node) in PYNODE_TO_NODE_ID_INFO:
        node_id += PYNODE_TO_NODE_ID_INFO[type(node)](node)
    return node_id


def py_load_ast(node, parent, table):
    """Converts python abstract syntax tree to a tree of ASTnodes."""
    node_id = py_load_node_id(node)
    ast_node = create_node(table, node_id, parent)
    ast_node.children = [py_load_ast(child, ast_node, table) for child in ast.iter_child_nodes(node)]
    ast_node.children = [child for child in ast_node.children if child is not None]
    return ast_node


def py_entropy(files, params):
    """Computes the Shannon entropy of Python ASTs."""
    table = IDtable()  # shared id table between file code trees
    super_root = ASTnode(-1, None)  # id set to -1, so no other node is matched w/ it
    for file in files:
        with open(file, 'rt') as f:
            root = ast.parse(f.read())
        super_root.children.append(py_load_ast(root, super_root, table))

    # use tree_sample_entropy functions to compute Shannon entropy
    size_of_groupings = build_tree_sample_groupings(super_root, params)
    topological_entropies = compute_topological_entropy(super_root, size_of_groupings, params)

    return topological_entropies


if __name__ == '__main__':
    a = ast.parse("func(a, b=c, *d, **e); a[0]=2; [i+2 for i in range(100)]")
    print(ast.dump(a))
    print(a.body, type(a), a._fields, a.body)

    table = IDtable()
    print_subtree(py_load_ast(a, None, table), 0, table)

