#!/usr/bin/env python

# front to end computes the Shannon entropy of a codebase (multiple of code modules/files)
# input:  list of code files
# output: Shannon entropy of all files merged

# turns each input file to a AST
# loads ASTs in memory, in a unified, language-agnostic format
# connects the root of all the files to a single super root
# runs the AST isomorphic complexity ("topological entropy") calculation
# outputs aggregated and tree-size wise entropy measures

# usage:    code_entropy.py <lang> [-max <K>] [-f] <file1> [<file2>] ...
# example:  code_entropy.py c++ -max 5 cpp_file1.cc cpp_file2.cc cpp_file3.cc ...

import sys
from cpp_entropy import cpp_entropy
from py_entropy import py_entropy

# TODO need a proper tree diffing algo to remove the included files from c++ code
# TODO pickle grouping to harddrive at each iteration, pickle stream in and out the groups..
# TODO ..never store the whole of it in memory (beside grouping1 and 2)

# TODO you may want a measure to compare how much of the entropy is made out of the repeated parts
# TODO add c++ compiler options (e.g. for files to include), as erroneous code is not interpreted at all
# TODO C#: https://github.com/dotnet/roslyn/wiki/Getting-Started-C%23-Syntax-Analysis
# TODO more options in how a node is defined, what it contains (e.g. do variable names matter?)
# TODO option to neglect certain types of nodes
# TODO support string and not just file inputs

LANGUAGES = {
    'c++': {
        'fun': cpp_entropy
    },
    'py': {
        'fun': py_entropy
    }
}


def main(args):
    if len(args) < 3:  # filename, language, first file
        print('Not enough arguments!')
        print('Example: code_entropy.py c++ -max 5 cpp_file1.cc cpp_file2.cc cpp_file3.cc ...')
        return

    params = {'max_k': -1, 'fast': False, 'rm_includes': False}  # defaults
    if '-max' in args:
        max_arg_i = args.index('-max')
        params['max_k'] = int(args[max_arg_i + 1])
        del args[max_arg_i]
        del args[max_arg_i]
    if '-f' in args:
        del args[args.index('-f')]
        params['fast'] = True
    if '-rmi' in args:  # TODO not yet functions properly
        del args[args.index('-rmi')]
        params['rm_includes'] = True

    lang = args[1]
    print('Selected language:', lang)
    cc = LANGUAGES[lang]['fun'](args[2:], params)

    print('Entropy at different sample tree sizes:', cc)
    print('Overall code Shannon entropy:', sum(cc))


if __name__ == '__main__':
    main(sys.argv)
