from hashlib import md5


class ASTnode(object):
    """Node of the original tree, of which complexity is assessed."""
    def __init__(self, node_id, parent):
        self.parent = parent  # if none: root
        self.children = []
        self.nid = node_id     # integer if possible

    def __eq__(self, other):
        return self.nid == other.nid


class IDtable:
    """Conversion table from AST node identification (strings) to numbers for ease of id comparison."""
    def __init__(self):
        self.t = {}   # table, str id -> int id
        self.rt = []  # reverse table, index -> str id


class MetaNode:
    """Node of a TreeSample."""
    def __init__(self, ast_node, meta_parent):
        if not isinstance(ast_node, ASTnode):
            raise TypeError("That's why it's meta.")

        self.ast_node = ast_node
        self.parent = meta_parent
        self.children = []

    def copy(self, parent):
        copied = MetaNode(self.ast_node, parent)
        copied.children = [child.copy(copied) for child in self.children]
        return copied

    def __eq__(self, other):
        if self.ast_node is other.ast_node:  # use _is_ so reference is compared
            return True
        return False

    def __hash__(self):
        return id(self.ast_node)


class TreeSample:
    """
    A tree (connected graph of nodes without cycles) inside the original abstract syntax tree.
    Not a subtree, as a tree sample does not necessary contain all nodes below its root.
    A tree sample can be compared to other tree samples pretty fast, that's why the redundant use of hashes.
    A tree sample first can be compared as the hash of its node set, then is compared as
    the hash of its nodes and topological structure.
    """
    def __init__(self, meta_root):
        self.meta_root = meta_root
        self.meta_node_set = {meta_node: meta_node for meta_node in iterative_depth_first_walk(meta_root)}
        self.hash = ""  # hash of asd node ids (for checking topological similarity)
        self.asd_obj_ids = ()
        self.py_real_hash = ""  # hash of all asd object's unique ids (for checking tree sample redundancy in groupings)
        self.md5_real_hash = ""  # same here, but used in the __eq__ method, not in __hash__ (more likely to be unique)
        self.update_hash()

    def __hash__(self):
        # note: only used when the topological structure is known to be the same
        return self.py_real_hash

    def __eq__(self, other):
        # note: only used when the topological structure is known to be the same
        return self.md5_real_hash == other.md5_real_hash

    def update_hash(self):
        bracket_notation = TreeSample.build_bracket_notation_recursive(self.meta_root)
        self.hash = md5()
        self.hash.update(bracket_notation.encode('utf-8'))
        self.hash = self.hash.hexdigest()  # [:10]

        self.asd_obj_ids = frozenset([id(meta_node.ast_node) for meta_node in iterative_depth_first_walk(self.meta_root)])
        self.py_real_hash = hash(self.asd_obj_ids)

        self.md5_real_hash = md5()
        for asd_obj_id in self.asd_obj_ids:
            self.md5_real_hash.update(str(asd_obj_id).encode('utf-8'))
        self.md5_real_hash = self.md5_real_hash.hexdigest()

    def copy(self):
        copied_root = self.meta_root.copy(None)  # copies whole tree of meta nodes
        return TreeSample(copied_root)

    def extend(self, ext):
        """
        Extend the tree by ext=TreeSample(parent->child), where parent is in tree.
        Here it is already known that parent is in meta_node_set and child is not,
        which is the criteria for extension.
        """
        # grab the corresponding "parent" element from the tree
        meta_parent_to_extend = self.meta_node_set[ext.meta_root]

        # insert the copy of the child (extension) to the right position
        # among the children of parent, according to the children ordering of the parent ast node
        children = meta_parent_to_extend.ast_node.children
        meta_children = meta_parent_to_extend.children
        child_to_insert = ext.meta_root.children[0].ast_node
        child_to_insert = MetaNode(child_to_insert, meta_parent_to_extend)

        inserted = False
        ast_i = 0   # iterator of the original ast array of children
        meta_i = 0  # iterator of the meta_parent_to_extend meta children, where the insertion goes
        while ast_i < len(children) and meta_i < len(meta_children):
            if child_to_insert.ast_node is children[ast_i]:
                meta_children.insert(meta_i, child_to_insert)
                inserted = True
                break

            if children[ast_i] is meta_children[meta_i].ast_node:
                meta_i += 1
            ast_i += 1

        if not inserted:
            meta_children.append(child_to_insert)

        self.meta_node_set[child_to_insert] = child_to_insert
        self.update_hash()

    @staticmethod
    def build_bracket_notation_recursive(meta_node):
        bracket_notation = str(meta_node.ast_node.nid)
        if len(meta_node.children) > 0:
            bracket_notation += '{'
            for child in meta_node.children:
                bracket_notation += TreeSample.build_bracket_notation_recursive(child)
            bracket_notation += '}'

        return bracket_notation


def breadth_first(tree):
    """Breadth first traversing."""
    yield tree
    last = tree
    for node in breadth_first(tree):
        for child in node.children:
            yield child
            last = child
        if last == node:
            return


def iterative_depth_first_walk(root):
    """Iterative depth first walk implementation."""
    to_expand = [root]
    while len(to_expand) > 0:
        node = to_expand.pop()
        to_expand.extend([child for child in node.children])
        yield node
    return


def create_node(table, node_id, parent):
    """
    Creates an ASTnode with the given id and parent.
    Overwrites the ID table.
    """
    # every new id gets a corresponding integer - fast comparison
    if node_id not in table.t:
        table.t[node_id] = len(table.rt)
        table.rt.append(node_id)

    return ASTnode(table.t[node_id], parent)


def subtract_tree(tree, part):  # FIXME need a proper tree diffing algo here
    """
    Subtracts part from tree.
    :param tree: tree to be subtracted from
    :param part: part to subtract
    :return: tree without part
    """
    # descend in both tree and part until the same nodes are found
    # descend down all of such nodes and remove their subtrees if they are identical
    tree_nodes = breadth_first(tree)
    part_nodes = breadth_first(part)

    # root doesn't count in comparison
    matching = []
    already_matched = []
    for part_child in part.children:
        for tree_child in tree.children:
            if part_child == tree_child and tree_child not in already_matched:
                already_matched.append(tree_child)
                matching.append((tree_child, part_child))
                break

    not_matching_anymore = []
    for i, match in enumerate(matching):
        for t, p in zip(iterative_depth_first_walk(match[0]), iterative_depth_first_walk(match[1])):
            if t != p:
                not_matching_anymore.append(i)
                break

    matching = [match[0] for i, match in enumerate(matching) if i not in not_matching_anymore]
    matching_by_parent = {}
    for match in matching:
        if id(match.parent) not in matching_by_parent:
            matching_by_parent[id(match.parent)] = set()
        m_i = [i for i, child in enumerate(match.parent.children) if child is match][0]
        matching_by_parent[id(match.parent)].add(m_i)

    # remove branches
    tosub = 0
    for match in matching_by_parent.items():
        for index in sorted(match[1], reverse=True):
            tosub += sum([1 for _ in iterative_depth_first_walk(tree.children[index])])
            del tree.children[index]  # FIXME removal of root children only
        # TODO remove the whole subtree
    print('taken', tosub)

    return tree


def print_subtree(node, lvl=0, table=None):
    """
    Depth first printing of a tree.
    Works with all ASTnode, MetaNode, and TreeSample objects.
    """
    node_id = 'invalid node'
    oid = 'NA'
    if isinstance(node, ASTnode):
        node_id = table.rt[node.nid] if table is not None else node.nid
        oid = id(node)
    elif isinstance(node, MetaNode):
        node_id = table.rt[node.ast_node.nid] if table is not None else node.ast_node.nid
        oid = id(node.ast_node)
    elif isinstance(node, TreeSample):
        node_id = table.rt[node.meta_root.ast_node.nid] if table is not None else node.meta_root.ast_node.nid
        node = node.meta_root
        oid = id(node.ast_node)
    print(('-' * lvl) + str(node_id), oid)
    
    for child in node.children:
        print_subtree(child, lvl + 1, table)


if __name__ == '__main__':
    table = IDtable()
    root = create_node(table, 0, None)
    root.children.append(create_node(table, 1, root))
    root.children.append(create_node(table, 0, root))
    root.children.append(create_node(table, 111, root))

    root.children[1].children.append(create_node(table, 1, root.children[1]))

    root.children[2].children.append(create_node(table, 1, root.children[2]))
    root.children[2].children.append(create_node(table, 0, root.children[2]))

    root2 = create_node(table, 10, None)
    root2.children.append(create_node(table, 111, root))
    root2.children[0].children.append(create_node(table, 1, root2.children[0]))
    root2.children[0].children.append(create_node(table, 0, root2.children[0]))

    print('tree:')
    print_subtree(root, 0, table)
    print('to remove:')
    print_subtree(root2, 0, table)

    purged_tree = subtract_tree(root, root2)
    print('purged:')
    print_subtree(purged_tree)
