import math
from scipy.misc import comb
from abstract_ast import *
from numpy import histogram
import numpy as np
from collections import deque


cmp_fun = lambda a,b: (a > b) - (a < b)  # py2 cmp function


def next_permutation(seq, pred=cmp_fun):
    """
    Like C++ std::next_permutation() but implemented as generator. Yields copies of seq.
    From: http://blog.bjrn.se/2008/04/lexicographic-permutations-using.html
    """
    def reverse(seq, start, end):
        # seq = seq[:start] + reversed(seq[start:end]) + \
        #       seq[end:]
        end -= 1
        if end <= start:
            return
        while True:
            seq[start], seq[end] = seq[end], seq[start]
            if start == end or start+1 == end:
                return
            start += 1
            end -= 1

    first = 0
    last = len(seq)
    seq = seq[:]

    # Yield input sequence as the STL version is often
    # used inside do {} while.
    yield seq[:]
    if last == 1:
        raise StopIteration
    while True:
        next = last - 1
        while True:
            # Step 1.
            next1 = next
            next -= 1
            if pred(seq[next], seq[next1]) < 0:
                # Step 2.
                mid = last - 1
                while not (pred(seq[next], seq[mid]) < 0):
                    mid -= 1
                seq[next], seq[mid] = seq[mid], seq[next]
                # Step 3.
                reverse(seq, next1, last)
                # Change to yield references to get rid of
                # (at worst) |seq|! copy operations.
                yield seq[:]
                break
            if next == first:
                raise StopIteration


def subset_sum(numbers, target, partial=[], partial_sum=0):
    """
    Find all combinations of a list of numbers with a given sum.
    Taken from: https://stackoverflow.com/questions/34517540/find-all-combinations-of-a-list-of-numbers-with-a-given-sum/34518260#34518260
    """
    if partial_sum == target:
        yield partial
    if partial_sum >= target:
        return
    for i, n in enumerate(numbers):
        remaining = numbers[i + 1:]
        for res in subset_sum(remaining, target, partial + [n], partial_sum + n):
            yield res


def ntreesample_(r, k):
    """
    Does all the per node heavy lifting of tree sample counting.
    Counts the number of possible tree samples all having their root at r.
    :param r: root of the counted tree samples
    :param k: number of vertexes in the counted tree samples, always >1
    """
    m = len(r.children)
    tree_sample_sizes = np.zeros(k, dtype=int)  # indexed by the size of tree sample in which root is not included
    tree_sample_sizes[0] = 1  # there is only 1 tree sample of size 1
    tree_sample_sizes[1] = m  # there is as many tree samples rooted at r of size 2 as children of r

    if m > 0 and k > 2:
        summed_ts_sizes = np.sum([child.tree_sample_sizes for child in r.children], 0) if m > 0 else np.zeros(k)
        num_of_children_w_ts_sizes = np.sum([child.tree_sample_sizes > 0 for child in r.children], 0) if m > 0 else np.zeros(k)
        # to sum up to 2 (=k-1, when k=3), you can either have 1+1 or just 2
        # 1+1 is done by selecting two children out of all, order doesn't matter (m choose 2, where m = len(children))
        # the number of ways 2 sized tree samples can be selected from the children is given by the summed tree samples sizes of 2
        tree_sample_sizes[2] = comb(m, 2) + summed_ts_sizes[1]

        # when k>3, things get more complicated
        # gather all the tree sample sizes of the children that will be used to sum up to k-1
        numbers_to_sum = []
        for i, num in enumerate(summed_ts_sizes):
            numbers_to_sum.extend([i + 1] * num)  # note: sorted

        for i, s in enumerate(r.tree_sample_sizes, 0):
            tree_sample_sizes[i] = s

        for i in range(max(3, len(r.tree_sample_sizes)), k):  # from here, k-1 (=the number to sum up to) becomes i
            # compute combinations of numbers that sums up to k-1
            # use only those numbers in the sum, for which we have corresponding tree sample sizes
            sums = [list(x) for x in set(tuple(x) for x in subset_sum(numbers_to_sum, i))]
            for sum_nums in sums:
                num_counts = np.array([0] * i)
                for _, num in enumerate(sum_nums):  # turns 1+2+3+3, that is [1,2,3,3], into [1,1,2]
                    num_counts[num - 1] += 1

                # when using a 3 sized tree sample, we effectively use a potential 2 and 1 sized tree sample as well
                # so the counts has to be accumulated starting from the last element until the first
                # from [1,1,2] it makes [4,3,2]; using this sequence we can check whether we have enough tree samples below
                accum_num_counts = np.cumsum(num_counts[::-1])[::-1]
                # have to have enough
                valid = np.all(num_of_children_w_ts_sizes[:i] - accum_num_counts >= 0)

                if valid:
                    # find all m long permutations of this valid summation
                    # fill the m - len(sum_nums) places in the permutation with 0s: corresponding children are left out
                    permutation_input = [0] * (m - len(sum_nums)) + sum_nums  # note: also sorted
                    for perm in next_permutation(permutation_input):  # requires sorted array, which it is
                        # check if the perm is valid: all children have the needed amount of tree samples
                        # check can be ignored by just computing the product that will end up being 0, if not valid
                        ts_sizes_product = np.prod([child.tree_sample_sizes[needed_size - 1]
                                                         for needed_size, child in zip(perm, r.children)
                                                         if needed_size > 0])
                        tree_sample_sizes[i] += ts_sizes_product  # finally add it to the tree sample count

    r.tree_sample_sizes = tree_sample_sizes

    # tag parent that this child has been processed
    if r.parent is not None:
        r.parent.nchild_processed += 1

        # if num of tags == num of children, ascend
        if r.parent.nchild_processed == len(r.parent.children):
            ntreesample_(r.parent, k)


def ntreesample(root, k):
    """
    Counts the number of all possible connected tree samples of size k of the whole tree.
    From: https://www.reddit.com/r/learnprogramming/comments/1v38n8/dynamic_programming_number_of_subtrees_of_size_k/
    :param root: of the AST
    :param k: tree sample size
    :return: number of connected tree samples of size k
    """

    # if k == 1, the number of 1 long tree samples is simply the number of nodes
    if k == 1:
        nnode = 0
        for node in iterative_depth_first_walk(root):
            nnode += 1
        return nnode

    # check if root is marked = if tree sample sizes have been computed of k or higher
    # then no extra computation is needed, just gather the tree sample size data already in the node
    if hasattr(root, 'computed') and root.computed >= k:
        result = 0
        for v in iterative_depth_first_walk(root):
            result += v.tree_sample_sizes[k - 1]

        return result

    # get the leaves and add a tag for later usage in ntreesample_
    leaves = []
    for node in iterative_depth_first_walk(root):
        if len(node.children) == 0:
            leaves.append(node)
        else:
            node.nchild_processed = 0  # to count how many children has been processed upstream in ntreesample_ function

    # run the algo recursively starting on each leaf
    for leaf in leaves:
        ntreesample_(leaf, k)

    # sweep through the tree again to sum up tree sizes of k-1
    result = 0
    for v in iterative_depth_first_walk(root):
        result += v.tree_sample_sizes[k - 1]

    # mark root to know that we have already counted tree samples up to size k
    root.computed = k

    return result


def gen_mini_tree(k, depth_samples, degree_samples):
    """
    Generates a mini tree that is somewhat represents an average tree sample in the original AST.
    Samples from depth_samples that defines the structure of the tree.
    At each node, the degree of that node is sampled (if not leaf) from degree_samples, and nodes are generated as its
    children as many, as many degree is sampled.
    :param k: min number of nodes needed in the minitree
    :param depth_samples: number of children nodes at different depths of the original tree
    :param degree_samples: degree samples of non-leaf nodes of the original AST
    :return: root of an ASTnode mini tree, always have #nodes >= k
    """
    # sample from the depth distribution to get a sample for approximate tree structure
    # depth values at different layers may be more than the sampled one in the generated mini tree, but not less
    # e.g. if at depth 3, there should be 4 nodes, there are going to be >= 4 nodes
    depth = [0]
    while sum(depth) < k:
        depth = depth_samples[np.random.choice(np.arange(len(depth_samples)))].copy()
    layers = [[] for _ in range(sum(depth > 0))]

    # insert the root
    ast_root = ASTnode(0, None)
    layers[0].append(ast_root)
    depth[0] = 0  # got the root

    # fulfill at all depths
    at_depth = 1
    at_parent = 0  # at_depth and at_parent determines the parent at which the children is generated
    while at_depth < len(layers):
        # sample from the degree distribution and add as many children, as the sample
        # may be more than the number defined in depth
        degree = np.random.choice(degree_samples)
        parent = layers[at_depth - 1][at_parent]
        gen_children = [ASTnode(at_depth * 100 + at_parent * 10 + i, parent) for i in range(degree)]
        parent.children.extend(gen_children)
        layers[at_depth].extend(gen_children)
        depth[at_depth] -= degree

        if depth[at_depth] <= 0:
            at_depth += 1
            at_parent = 0
        else:
            at_parent = (at_parent + 1) % len(layers[at_depth - 1])  # next parent if any

    return ast_root


def fast_ntreesample_(r, k, depth_samples):
    """
    TODO
    :param r:
    :param k:
    :param depth_samples:
    :param sample_prob:
    :return:
    """

    r.npaths_of_depth = deque([0] * k, maxlen=k)
    for child in r.children:
        for i, n in enumerate(child.npaths_of_depth):
            r.npaths_of_depth[i] += n
        del child.npaths_of_depth

    # add this node, shifting all the nodes to the right, removing too long paths >k
    r.npaths_of_depth.appendleft(1)

    # add sample
    if len(r.children) > 0:  # depth > 0, don't care about leaves, there are a bunch of them
        depth_samples.append(list(r.npaths_of_depth))

    # tag parent that this child has been processed
    if r.parent is not None:
        r.parent.nchild_processed += 1

        # if num of tags == num of children, ascend
        if r.parent.nchild_processed == len(r.parent.children):
            fast_ntreesample_(r.parent, k, depth_samples)


def fast_ntreesample(root, k, nmini_tree=100):
    """
    Approximates the number of all possible connected tree samples of size k of the whole tree.
    Registers the degree of each node in the tree and depth of randomly selected nodes.
    It aims to arrive at mini model tree of the whole AST. The ntreesample is then run on the mini tree, the result
    of which is multiplied by the number of nodes in the AST to get the ntreesample estimate.
    :param root: root of the AST
    :param k: tree sample size
    :return: approximate for the number of connected tree samples of size k
    """

    # if k == 1, the number of 1 long tree samples is simply the number of nodes
    nnode = 0
    nnotleaf = 0  # needed down below wither way
    for node in iterative_depth_first_walk(root):
        nnode += 1
        if len(node.children) > 0:
            nnotleaf += 1
    if k == 1:
        return nnode

    # get the leaves and node degree_samples
    leaves = []
    degree_samples = []
    for node in iterative_depth_first_walk(root):
        if len(node.children) > 0:
            degree_samples.append(len(node.children))  # too much leaves in ASTs, don't care about them lol
            node.nchild_processed = 0
        else:
            leaves.append(node)
    degree_samples = np.array(degree_samples)

    # ascend starting from the leaves and keep track of depths <= k, sampling from some of them
    depth_samples = []
    # sample_prob = ndepth_samples / len(degree_samples)
    for leaf in leaves:
        fast_ntreesample_(leaf, k, depth_samples)

    depth_samples = np.array(depth_samples)  # matrix N*(k-1)

    # generate a bunch of generic enough mini trees to compute ntreesample on
    nts_estimates = np.zeros(nmini_tree)
    for t_i in range(nmini_tree):
        mini_tree = gen_mini_tree(k, depth_samples, degree_samples)
        for node in iterative_depth_first_walk(mini_tree):
            node.tree_sample_sizes = []
        ntreesample(mini_tree, k)  # compute ntreesample on the mini tree
        nts_estimates[t_i] = mini_tree.tree_sample_sizes[k-1]  # number of tree samples branching from the root only

    # assume that each mini tree's node is an avg non-leaf node in the AST
    # multiply each ntreesample estimate by the number of nodes to get ntreesample estimate for the whole original AST
    # then take avg across estimates (or the other way around to be faster)
    return int(np.mean(nts_estimates) * nnotleaf)


def build_next_tree_sample_grouping(prev_grouping, grouping2):
    """
    In this context a group is multiple of trees with the same topological structure.
    Only those groups can stay in the next grouping that have at least 2 members (repeated substructures).
    This function only processes one iteration by using the grouping of the prev iteration.
    Grouping format: {id: [p1, p2, ..., pn], ...}, where id = node id when k=1, or hash of the
    bracket notation sample tree filled with node ids, and pk is the count of tree samples with that id, when k > 1.
    :param prev_grouping is the previous (k-1) grouping
    :param grouping2 is the 2-sized tree grouping. It is used to extend the tree samples (tree samples) of the previous grouping.
    This function only deals with k > 2. k = 1 and k = 2 tree groupings are performed in build_tree_sample_groupings.
    Returns the next grouping.
    """

    # TODO prev_grouping as file reference, containing pickled tree samples (not just duplicates)
    # TODO instead of new_grouping: groups of string hashes to identify duplicates: {tree.hash: {tree.md5_real_hash}}
    # TODO trees themselves are straight streamed out to file
    # TODO so when loading prev_grouping one-by-one, the hash grouping is checked whether it contains the tree.hash loaded
    # TODO for this all to work: ASTnode needs a unique real_id (generated by depth first walk indexing) instead of id(node)
    # TODO                       tree structure of meta nodes should be preserved when saving to file, because id() changes from run to run

    new_grouping = {}
    for sample_hash, trees in prev_grouping.items():
        # check if the grouping2 parent (=root) node is in the sample to extend,
        # but the child node of the same grouping2 sample is not in the sample to extend
        # then extend it, if true
        for tree in trees:
            for simple_hash2, trees2 in grouping2.items():
                for tree2 in trees2:
                    if tree2.meta_root in tree.meta_node_set and\
                       tree2.meta_root.children[0] not in tree.meta_node_set:
                        tree_cpy = tree.copy()
                        tree_cpy.extend(tree2)
                        if tree_cpy.hash not in new_grouping:
                            new_grouping[tree_cpy.hash] = set()
                        new_grouping[tree_cpy.hash].add(tree_cpy)
    
    return new_grouping


def build_tree_sample_groupings(ast_root, params):
    """
    Builds groupings with increasing tree sample length until no more topological similarity is found.
    A group is defined as multiple of tree samples (tree samples) having same topological properties (nodes with same id is
    connected in the same manner). A grouping consists of multiple groups, with tree samples of the same size.
    By starting from repeated single nodes and gradually increasing the size only keeping the repeated groups from the
    previous iteration, we can build up groupings of repeated tree samples, which in the end can be used to assess a
    certain kind of entropy of the given tree.
    :param ast_root root of the abstract syntax tree
    :param params parameters passed from command line
    :return Returns the size of groups for each grouping (sample_repetitions):
        [ [|group_11|, |group_12|, ...], [|group_21|, |group_22|, ...], ... ]
            where group_ij is the jth topological kind of all the same i long trees.
    """
    sample_repetitions = []
    until_ts_size = params['max_k']

    # assemble grouping of tree with length 1 = group nodes according to id
    grouping1 = {}
    for node in iterative_depth_first_walk(ast_root):
        if node.nid not in grouping1:
            grouping1[node.nid] = []
        # an ast tree is created with only one MetaNode in it
        grouping1[node.nid].append(TreeSample(MetaNode(node, None)))

    # select trees (nodes) that are repeated and add the group sizes to sample_repetitions
    grouping1 = {node_id: trees for node_id, trees in grouping1.items() if len(trees) > 1}
    sample_repetitions.append([len(trees) for node_id, trees in grouping1.items()])

    size_of_groupings = [[len(trees) for node_id, trees in grouping1.items()]]
    print('Grouping #1 done -', size_of_groupings[-1])
    print('Number of repeated nodes:', sum(size_of_groupings[-1]))

    # go for 2 long samples
    # for each sample (node) in grouping1, add each child to the tree sample
    grouping2 = {}
    for node_id, trees in grouping1.items():
        for sample in trees:
            # iterate over all children of the given selected grouping 1 node
            # and create a new tree sample with that child added to a new tree sample
            # that is, to a new 2 long tree sample
            # note, only those grouping1 samples are added that have at least 1 child
            for sample_root_child in sample.meta_root.ast_node.children:
                sample_root_cpy = sample.meta_root.copy(None)
                sample_root_cpy.children.append(MetaNode(sample_root_child, sample_root_cpy))
                two_long_sample = TreeSample(sample_root_cpy)
                
                if two_long_sample.hash not in grouping2:
                    grouping2[two_long_sample.hash] = set()
                grouping2[two_long_sample.hash].add(two_long_sample)
    
    # select groups having multiple samples (repetition of 2 long samples)
    grouping2 = {node_id: trees for node_id, trees in grouping2.items() if len(trees) > 1}

    size_of_groupings.append([len(trees) for node_id, trees in grouping2.items()])
    print('Grouping #2 done -', size_of_groupings[-1])
    print('Number of repeated tree samples:', sum(size_of_groupings[-1]))

    # build longer unique, distinct sample groupings, first copy grouping2, so it won't get overwritten
    k = 3
    current_grouping = {node_id: [tree.copy() for tree in trees] for node_id, trees in grouping2.items() if len(trees) > 1}
    while len(current_grouping) > 0 and k <= until_ts_size:
        current_grouping = build_next_tree_sample_grouping(current_grouping, grouping2)
        current_grouping = {node_id: trees for node_id, trees in current_grouping.items() if len(trees) > 1}
        if len(current_grouping) > 0:
            size_of_groupings.append([len(trees) for node_id, trees in current_grouping.items()])

            hist = histogram(size_of_groupings[-1], bins=range(min(size_of_groupings[-1]), max(size_of_groupings[-1]) + 2))
            print('Grouping #' + str(len(size_of_groupings)), 'done -', hist[0], '\n\tinstances of group size', hist[1][:-1])
            print('Number of repeated tree samples:', sum(size_of_groupings[-1]))
        k += 1

    return size_of_groupings


def compute_topological_entropy(ast_root, size_of_groupings, params):
    """
    Returns the entropy value for each size of tree sample, by using the precomputed list of repeated connected tree samples.
    Entropy here is defined for each tree sample length. Let the length be 5, in that case each 5 long possible tree sample of
    the given tree is taken as a symbol, the enumeration of all these symbols constitutes to the code of the tree. By
    assessing the repetition of these symbols, we can derive the entropy of the tree, similarly as we do with strings.
    The name "topological entropy" aims to signal that only the topology of the graph is taken into account.
    In fact, only the topology of connected tree samples in a tree is taken into account (e.g. two distant vertices without
    an edge between is not considered a connected tree sample, thus its repetition is not assessed).
    :param ast_root root of the abstract syntax tree
    :param size_of_groupings gropings of tree samples as computed by build_tree_sample_groupings
    :param params parameters passed from command line
    """
    k = 1
    entropies = []
    until_ts_size = params['max_k']

    # biggest tree sample is the one containing the whole tree
    tree_size = 0
    for node in iterative_depth_first_walk(ast_root):
        node.tree_sample_sizes = []
        tree_size += 1

    # pre-compute until size tree_size
    if until_ts_size < 0:
        until_ts_size = tree_size

    used_ntreesample = fast_ntreesample if params['fast'] else ntreesample

    for group_sizes in size_of_groupings:
        ntree_sample_of_k_size = used_ntreesample(ast_root, k)
        ntree_sample_repeated = sum(group_sizes)
        entropy_sum = 0

        print('k =', k, 'ntreesample', ntree_sample_of_k_size)

        # add entropy of repeated tree samples
        for group_size in group_sizes:
            p = float(group_size) / ntree_sample_of_k_size
            entropy_sum += p * math.log(p, 2)
        
        # add the entropy content of the rest (not repeated tree samples)
        p_rest = 1. / ntree_sample_of_k_size
        entropy_sum += (ntree_sample_of_k_size - ntree_sample_repeated) * p_rest * math.log(p_rest, 2)
        
        entropies.append(-entropy_sum)
        k += 1

        if k > until_ts_size:
            break

    # now increase tree sample size until we reach the size of the whole tree (or the max k given)
    # and consider each tree sample to be unique (as they are)
    for k_ in range(k, until_ts_size + 1):
        ntree_sample_of_k_size = used_ntreesample(ast_root, k_)  # note: this can be approximated, if tree is too large

        print('k =', k_, 'ntreesample', ntree_sample_of_k_size)

        p = 1. / ntree_sample_of_k_size
        entropies.append(-math.log(p, 2))
    
    return entropies


if __name__ == '__main__':
    table = IDtable()
    root = create_node(table, 0, None)
    root.children.append(create_node(table, 1, root))
    root.children.append(create_node(table, 0, root))
    root.children.append(create_node(table, 0, root))

    root.children[1].children.append(create_node(table, 1, root.children[1]))

    root.children[2].children.append(create_node(table, 1, root.children[2]))
    root.children[2].children.append(create_node(table, 0, root.children[2]))

    print_subtree(root, 0, table)

    print('anticipated values:      6 7 8 7 4 1')
    print('nsampletree 2,3,4,5,6,7:', ntreesample(root, 2), ntreesample(root, 3), ntreesample(root, 4), ntreesample(root, 5), ntreesample(root, 6), ntreesample(root, 7))
