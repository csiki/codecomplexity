import os
import subprocess
from abstract_ast import *
from tree_sample_entropy import *


def cpp_load_node_id(line):
    """Converts LLVM AST nodes to generic ASTnode."""
    dash = line.index('-')
    typ = line[dash + 1:line.find(' ', dash)]
    detail = line[line.rfind('>') + 2:]  # TODO check if this holds for all node types FIXME it doesn't, it contains line:col bullshit
    return typ + ' ' + detail


# returns (root of abstract asd, id table )
def cpp_load_ast(dump, table):
    """Converts LLVM abstract syntax tree to a tree of ASTnodes."""

    # first line always the same
    lines = dump.decode().split('\n')
    if 'TranslationUnitDecl' not in lines[0]:
        raise RuntimeError('invalid dump!')

    root = create_node(table, 'TranslationUnitDecl', None)

    # iterate line by line while building the tree
    path_stack = [root]
    for line in lines[1:]:
        if len(line) == 0:
            break
        lvl = (line.index('-') + 1) // 2  # where TranslationUnitDecl is lvl=0
        path_stack = path_stack[:lvl]  # if lvl=1, that means TranslationUnitDecl stays only in stack
        node_id = cpp_load_node_id(line)
        child = create_node(table, node_id, path_stack[-1])
        path_stack[-1].children.append(child)  # add to children
        path_stack.append(child)  # sorta depth first

    return root


# duplicate files
# edit duplicated files
# remove lines starting with #include
# run llvm subprocess to dump ast for each file
# load each ast into own format
# connect ast roots to a super root
# compute code entropy
def cpp_entropy(files, params):
    """Computes the Shannon entropy of LLVM ASTs."""

    ASTs = []
    table = IDtable()  # shared id table between file code trees
    for file in files:

        # create AST out of code
        dump = subprocess.Popen("clang++ -Xclang -ast-dump -fsyntax-only -fno-color-diagnostics " + file,
                                shell=True, stdout=subprocess.PIPE).stdout.read()
        ast = cpp_load_ast(dump, table)

        # remove parts of the tree containing the included units
        if params['rm_includes']:
            # copy
            fname, ext = os.path.splitext(file)
            includes_file = fname + "_inc" + ext

            # add includes to a separate file
            with open(file, 'r') as orig_f:
                includes = [line for line in orig_f if '#include' in line]

            with open(includes_file, 'w+') as cleared_f:
                cleared_f.writelines(includes)

            # dump ast
            dump_inc = subprocess.Popen("clang++ -Xclang -ast-dump -fsyntax-only -fno-color-diagnostics " + includes_file,
                                        shell=True, stdout=subprocess.PIPE).stdout.read()
            # os.remove(includes_file) TODO

            # subtract the included parts
            part_ast = cpp_load_ast(dump_inc, table)
            print(sum([1 for _ in iterative_depth_first_walk(part_ast)]))
            print(sum([1 for _ in iterative_depth_first_walk(ast)]))
            # print_subtree(part_ast)
            ast = subtract_tree(ast, part_ast)
            print(sum([1 for _ in iterative_depth_first_walk(ast)]))

        ASTs.append(ast)

    # connect trees
    super_root = ASTnode(-1, None)  # id set to -1, so no other node is matched w/ it
    for root in ASTs:
        root.parent = super_root
        super_root.children.append(root)

    # print_subtree(super_root, 0, table)

    # use tree_sample_entropy functions to compute Shannon entropy
    size_of_groupings = build_tree_sample_groupings(super_root, params)
    topological_entropies = compute_topological_entropy(super_root, size_of_groupings, params)
    
    return topological_entropies


if __name__ == '__main__':
    e = cpp_entropy(['cpp/test.cc'], {'max_k': 5, 'rm_includes': True})
    pass
